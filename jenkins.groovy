pipeline{
	
	agent any
	
	stages{
	
		stage("checking maven version"){
		    agent{
				node{
					label("maven")
				}
			}
	steps{
			sh 'mvn --version'
			}
		}
		stage("checking java version"){
		    agent{
				node{
					label("nexus")
				}
			}
			steps{
			sh 'java -version'
			sh 'hostname'
			}
		}
	}

}
